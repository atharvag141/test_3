const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const port = 4000;

// Middleware to parse JSON and URL-encoded bodies
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Serve the index.html file for the root URL
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Define a simple form submission route
app.post('/submit', (req, res) => {
  const { name, email } = req.body;

  // Read existing data from the database file
  const dataPath = __dirname + '/data/db.json';
  let existingData = [];
  try {
    const data = fs.readFileSync(dataPath, 'utf8');
    existingData = JSON.parse(data);
  } catch (error) {
    console.error('Error reading data from the database file:', error);
  }

  // Add new data to the database
  const newData = { name, email };
  existingData.push(newData);

  // Write the updated data back to the database file
  try {
    fs.writeFileSync(dataPath, JSON.stringify(existingData, null, 2));
  } catch (error) {
    console.error('Error writing data to the database file:', error);
  }

  res.send('Form submitted successfully!');
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
