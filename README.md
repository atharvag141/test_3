# Node.js Application Jenkins Build

This repository contains a simple Node.js application and a Jenkins pipeline configuration for building, testing, and deploying the application on a Windows environment.

## Prerequisites

- [Jenkins](https://www.jenkins.io/download/) installed and configured on a Windows machine.
- [Git](https://git-scm.com/downloads) installed.
- [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.

## Jenkins Job Configuration

1. Create a Freestyle project in Jenkins.
2. Configure the Source Code Management to use Git and provide the repository URL.
3. Add a "Build" step using "Execute Windows batch command."
4. Use the following build script in the command field:

    ```batch
    set PATH=%PATH%;C:\Program Files\nodejs
    npm install express
    node app.js 
    ```

5. Save the configuration and run the Jenkins job.

## Notes

- This script installs Node.js using the Node.js MSI installer.
- It adds Node.js and npm to the PATH for accessibility.
- Adjust the Node.js version and other details in the script as needed.

